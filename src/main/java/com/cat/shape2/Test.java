/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.shape2;

/**
 *
 * @author Black Dragon
 */
public class Test {
    public static void main(String[] args) {
        Circle circle = new Circle(7);
        circle.calArea();
        
        Triangle tri = new Triangle(5,4);
        tri.calArea();
        
        Rectangle rect = new Rectangle(8,2);
        rect.calArea();
        
        Square squ = new Square(3);
        squ.calArea();
    }
}
