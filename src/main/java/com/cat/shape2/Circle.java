/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.shape2;

/**
 *
 * @author Black Dragon
 */
public class Circle extends Shape {
    private double r;
    private double pi = 22.0/7;
    public Circle(double r) {
        System.out.println("Create Circle radius: " + r);
        this.r = r;
    }

    @Override
    public void calArea() {
        double ans = r*r*pi;
        System.out.println("Area of Circle is: " + ans);
    }
    
    @Override
    public void print() {
        System.out.println("Circle radius: " + r);
    }
}
