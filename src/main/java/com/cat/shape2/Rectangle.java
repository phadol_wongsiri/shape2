/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.shape2;

/**
 *
 * @author Black Dragon
 */
public class Rectangle extends Shape {
    private double width;
    private double height;
    
    public Rectangle(double width, double height) {
        if(width == height){
            System.out.println("Create Square with side: " + width);
        }else{
            System.out.println("Create Rectangle with width: " + width + 
                " and height: " + height);
        }
        this.width = width;
        this.height = height;
    }
    
    @Override
    public void calArea() {
        double ans = width*height;
        System.out.println("Area of Rectangle is: " + ans);
    }
    
    @Override
    public void print() {
        
        if(width == height){
            System.out.println("Square with side: " + width);
        }else{
            System.out.println("Rectangle with width: " + width + 
                " and height: " + height);
        }
    }
}
