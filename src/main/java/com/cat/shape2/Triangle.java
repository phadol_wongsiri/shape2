/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.shape2;

/**
 *
 * @author Black Dragon
 */
public class Triangle extends Shape {
    private double base;
    private double height;
    
    public Triangle(double base, double height) {
        System.out.println("Create Triangle with base: " + base + 
                " and height: " + height);
        this.base = base;
        this.height = height;
    }
    
    @Override
    public void calArea() {
        double ans = base * height / 2;
        System.out.println("Area of Triangle is: " + ans);
    }
    
    @Override
    public void print() {
        System.out.println("Triangle with base: " + base + 
                " and height: " + height);
    }
}
